package az.ingress.jpam10.domain;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "ing_group")
public class Group {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
}
