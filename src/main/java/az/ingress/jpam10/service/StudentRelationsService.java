package az.ingress.jpam10.service;

import az.ingress.jpam10.domain.Book;
import az.ingress.jpam10.domain.Group;
import az.ingress.jpam10.domain.Student;
import az.ingress.jpam10.domain.StudentDetail;
import az.ingress.jpam10.respository.StudentDetailRepository;
import az.ingress.jpam10.respository.StudentRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class StudentRelationsService {

    private final StudentRepository studentRepository;
    private final StudentDetailRepository studentDetailRepository;

    //@PostConstruct
    public void createStudentOneToOne() {
        log.trace("Creating student");
        StudentDetail studentDetail = StudentDetail.builder()
                .bio("Born in Nakchivan")
                .build();
        log.trace("Student details before save {}", studentDetail);
        //studentDetailRepository.save(studentDetail);
        log.trace("Student details after save {}", studentDetail);

        Student student = Student.builder()
                .studentNumber("32674523")
                .firstName("Orkhan")
                .lastName("Safarov")
                .studentDetail(studentDetail)
                .age(20)
                .build();
        studentRepository.save(student);
    }


   // @PostConstruct
    public void createStudentOneToMany() {
        for (int i = 0; i < 10; i++) {
            StudentDetail studentDetail = StudentDetail.builder()
                    .bio("Born in Nakchivan")
                    .build();

            Student student = Student.builder()
                    .studentNumber("32674523")
                    .firstName("Orkhan" + i)
                    .lastName("Safarov" + i)
                    .studentDetail(studentDetail)
                    .age(20)
                    .build();

            Book book1 = Book.builder()
                    .isbn("23432")
                    .name("Jpa Tutorial" + i)
                    .student(student)
                    .build();

            Book book2 = Book.builder()
                    .isbn("345764")
                    .name("JSP Tutorial" + i)
                    .student(student)
                    .build();
            student.setBooks(List.of(book1, book2));

            studentRepository.save(student);
        }
    }

    //@PostConstruct
    public void createStudentManyToMany() {
        StudentDetail studentDetail = StudentDetail.builder()
                .bio("Born in Nakchivan")
                .build();

        Student student = Student.builder()
                .studentNumber("32674523")
                .firstName("Orkhan")
                .lastName("Safarov")
                .studentDetail(studentDetail)
                .age(20)
                .build();

        Book book1 = Book.builder()
                .isbn("23432")
                .name("Jpa Tutorial")
                .student(student)
                .build();

        Book book2 = Book.builder()
                .isbn("345764")
                .name("JSP Tutorial")
                .student(student)
                .build();
        Group group1 = new Group();
        group1.setName("MS10");

        Group group2 = new Group();
        group2.setName("MS9");

        student.setGroups(List.of(group1, group2));

        student.setBooks(List.of(book1, book2));

        studentRepository.save(student);
    }
}
