package az.ingress.jpam10.service;

import az.ingress.jpam10.respository.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class StudentServiceFetchTypes {

    private final StudentRepository repository;

    //@PostConstruct
   // @Transactional
    public void getStudents() {
        // repository.findAll()
        //         .forEach(System.out::println);

        repository.findAllJoinFetch()
                .forEach(System.out::println);
    }

}
