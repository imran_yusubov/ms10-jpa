package az.ingress.jpam10.service;

import az.ingress.jpam10.domain.Student;
import az.ingress.jpam10.domain.Student_;
import az.ingress.jpam10.dto.StudentDto;
import az.ingress.jpam10.respository.BookRepository;
import az.ingress.jpam10.respository.StudentRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.internal.SessionImpl;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
@RequiredArgsConstructor
public class StudentService {

    private final EntityManagerFactory entityManagerFactory; //FactoryDesignPatter
    private final EntityManager entityManage;

    //private final JPAStreamer jpaStreamer;
    private final StudentRepository studentRepository;
    private final ModelMapper modelMapper;
    private final BookRepository bookRepository;

    //https://docs.spring.io/spring-data/jpa/docs/current/reference/html/
    //@PostConstruct
    public void listByQueryMethods() {
        studentRepository.findByFirstName("Orkhan")
                .stream()
                .forEach(System.out::println);

        studentRepository.findByFirstNameAndLastName("Orkhan", "Safarov")
                .stream()
                .forEach(System.out::println);

        studentRepository.findByFirstNameAndLastName(null, "Safarov")
                .stream()
                .forEach(System.out::println);
    }

    //@PostConstruct
    public void jpql() {
        studentRepository.findStudentsWithJpql("Orkhan", "Safarov")
                .stream()
                .forEach(System.out::println);
    }

    //@PostConstruct
    public void nativeSql() {
        studentRepository.findStudentsWithNativeSql("Orkhan", "Safarov")
                .stream()
                .forEach(System.out::println);
    }

    /*
        Locking
     */
    //@PostConstruct
    @Transactional(rollbackFor = Exception.class, isolation = Isolation.READ_UNCOMMITTED)
    //proxy pattern, aspectj, cglib, aspect,
    public void createStudentsTransactional() throws Exception {
        Student student2 = new Student(); //new -> perist save()
        student2.setId(2l);
        Student student = studentRepository.findById(1l)
                .get(); //
        student.setFirstName("Elgun"); //managed mode -> persist
        studentRepository.save(student);
        student2.setFirstName("Rasul");
        studentRepository.save(student2); //managed
        student2.setFirstName("wwjhegfiew");
        student2.setFirstName("Abdulla");
        studentRepository.save(student2);
    }


    //@PostConstruct
    public void jpaStreamer() {
//        jpaStreamer.stream(Student.class) // Film.class is the @Entity representing the film-table
//                .filter(Student$.firstName.equal("Orkhan"))
//                .limit(5)
//                .forEach(System.out::println);
    }

    //@PostConstruct
    //http://localhost:8080/students?firstName=Orkhan&lastName=Safarov&age=20
    //All fields are optional, and if something is supplied then we will use it for filtering
    //you are not allowed to use if else
    public void homeWork() {
//        jpaStreamer.stream(Student.class) // Film.class is the @Entity representing the film-table
//                .filter(Student$.firstName.equal("Orkhan"))
//                .limit(5)
//                .forEach(System.out::println);
    }

    //@PostConstruct
    public void getStudentsWithParams() {
        log.trace("Loading students with params");
        studentRepository.getStudentsWithParam("Orkhan", null, 20)
                .forEach(System.out::println);
    }

    // @PostConstruct
    public Page<StudentDto> getStudentsWithSpecificationExecutor(Map<String, String> searchFilter, Pageable pageable) {
        log.trace("Loading students with JPA spec executor");
//        studentRepository.findAll(getStudentsSpec("Orkhan", null))
//                .forEach(System.out::println);
//        return studentRepository.findAll(getStudentsSpec(Map.of(Student_.FIRST_NAME, "Orkhan",
//                Student_.LAST_NAME, "Safarov")));
        searchFilter.remove("page");
        searchFilter.remove("size");
        searchFilter.remove("sort");
        return studentRepository.findAll(getStudentsSpec(searchFilter), pageable)
                .map(c -> modelMapper.map(c, StudentDto.class));
    }

    public Page<Student> getStudentsWithPagination(Pageable pageable) {
        return studentRepository.findAll(pageable);
    }

    private Specification<Student> getStudentsSpec(String firstName, String lastName) {
        return (root, query, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();

            if (StringUtils.hasText(firstName)) {
                Predicate firstNamePred = criteriaBuilder.equal(root.get(Student_.FIRST_NAME), firstName);
                predicates.add(firstNamePred);
            }

            if (StringUtils.hasText(lastName)) {
                predicates.add(criteriaBuilder.equal(root.get(Student_.LAST_NAME), lastName));
            }

            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
    }

    private Specification<Student> getStudentsSpec(Map<String, String> queryMap) {
        return (root, query, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();

            queryMap.keySet()
                    .forEach(key -> {
                        if (StringUtils.hasText(queryMap.get(key))) {
                            predicates.add(criteriaBuilder.equal(root.get(key), queryMap.get(key)));
                        }
                    });

            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
    }

    /*
        Git
        Spring, creational Deign Patterns
        Dependency Injection
        Http & Rest, Idempotent Safe
        Rest Controller
        AOP
        Docker
        Docker Compose
        Gradle
     */

    // Service3 implenst A,B   ASerive BService
    // ServiceA
    // ServiceB
    // loginA() ->serviceA.loginA();
    // logoutB() -> serviceB.logOutB();
}
