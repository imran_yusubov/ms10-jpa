package az.ingress.jpam10.service;

import az.ingress.jpam10.domain.Student;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;

@Slf4j
@Service
@RequiredArgsConstructor
public class StudentEntityManagerExample {

    //@PersistenceContext
    private final EntityManagerFactory entityManagerFactory;


    @PostConstruct
    public void createStudentWithEM() {
        log.info("Saving with EM");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        Student student = new Student();
        student.setFirstName("Rasul");
        entityManager.persist(student);

    }


}
