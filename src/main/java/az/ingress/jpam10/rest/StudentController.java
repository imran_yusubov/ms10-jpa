package az.ingress.jpam10.rest;

import az.ingress.jpam10.domain.Student;
import az.ingress.jpam10.dto.StudentDto;
import az.ingress.jpam10.service.StudentService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@Slf4j
@RestController
@RequestMapping("students")
@RequiredArgsConstructor
public class StudentController {

    private final StudentService service;

    @GetMapping
    public Page<StudentDto> getStudents(@RequestParam Map<String, String> searchFilter, Pageable pageable) {
        log.trace("Getting students");
        searchFilter.forEach((key, value) -> log.trace("key is {}, value is {}", key, value));
        return service.getStudentsWithSpecificationExecutor(searchFilter, pageable);
    }

    @GetMapping("/page")
    public Page<Student> getStudents(Pageable pageable) {
        log.trace("Getting students");
        return service.getStudentsWithPagination(pageable);
    }
}
