package az.ingress.jpam10.respository;

import az.ingress.jpam10.domain.StudentDetail;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentDetailRepository extends JpaRepository<StudentDetail, Long> {
}
