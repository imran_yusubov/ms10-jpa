package az.ingress.jpam10.respository;

import az.ingress.jpam10.domain.Book;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookRepository extends JpaRepository<Book,Long> {
}
