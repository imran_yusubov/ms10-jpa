package az.ingress.jpam10.respository;

import az.ingress.jpam10.domain.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface StudentRepository extends JpaRepository<Student, Long>, JpaSpecificationExecutor<Student> {

    List<Student> findByFirstName(String orkhan); //select * from student where first_name='Orkhan'

    List<Student> findByFirstNameAndLastName(String orkhan, String safarov);

    List<Student> findByLastName(String safarov);

    @Query(value = "SELECT s from Student s where (:firstName is null or s.firstName = :firstName) " +
            "and (:lastName is null or s.lastName = :lastName) " +
            "and (:age is null or s.age = :age)")
    List<Student> getStudentsWithParam(@Param("firstName") String firstName,
                                       @Param("lastName") String lastName,
                                       @Param("age") Integer age);

    @Query("SELECT s FROM Student s WHERE s.firstName = ?1 AND s.lastName= ?2")
    List<Student> findStudentsWithJpql(String firstName, String lastName);

    @Query(nativeQuery = true, value = "SELECT * FROM our_students s WHERE s.first_name = ?1 AND s.last_name= ?2")
    List<Student> findStudentsWithNativeSql(String orkhan, String safarov);


    @Query("SELECT student FROM Student student LEFT JOIN FETCH student.groups igroup")
    List<Student> findAllJoinFetch();

}
