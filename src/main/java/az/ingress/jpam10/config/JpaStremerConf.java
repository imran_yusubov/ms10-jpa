package az.ingress.jpam10.config;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.persistence.EntityManagerFactory;

@Configuration
@RequiredArgsConstructor
public class JpaStremerConf {

    private final EntityManagerFactory entityManagerFactory;

//    @Bean
//    public JPAStreamer jpaStreamer() {
//        return JPAStreamer.of(entityManagerFactory);
//    }
}
