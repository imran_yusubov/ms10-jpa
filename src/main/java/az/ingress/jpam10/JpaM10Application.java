package az.ingress.jpam10;

import az.ingress.jpam10.domain.Student;
import az.ingress.jpam10.respository.StudentRepository;
import az.ingress.jpam10.service.StudentService;
import az.ingress.jpam10.service.StudentServiceFetchTypes;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@RequiredArgsConstructor
@SpringBootApplication
public class JpaM10Application implements CommandLineRunner {

    private final StudentRepository repository;
    private final StudentService studentService;
    private final StudentServiceFetchTypes studentServiceFetchTypes;

    public static void main(String[] args) {
        SpringApplication.run(JpaM10Application.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
//        Student rasul = Student
//                .builder()
//                .age(20)
//                .firstName("Rasul")
//                .lastName("Musayev")
//                .studentNumber("234")
//                .build();
//
//        Student orkhan = Student
//                .builder()
//                .age(20)
//                .firstName("Orkhan")
//                .lastName("Safarov")
//                .studentNumber("133")
//                .build();
//
//        repository.save(orkhan);
//        repository.save(rasul);

        //studentServiceFetchTypes.getStudents();

        //  studentService.createStudentsTransactional();

        repository.findById(1l)
                .get();
    }
}
